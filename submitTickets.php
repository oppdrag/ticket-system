<?php
include "backend/getFromDatabase.php";
include "backend/submitFunctions.php";

@session_start();
if (!isset($_SESSION['user'])) {
    header("Location: login.php");
    die();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $ticketId = submitFunctions::submitTicket($_POST['title'], $_POST['description'], $_POST['category']);
    echo "<h3>Ticket submit</h3>";
    echo "<h3>Id: $ticketId</h3>";
    echo "<p>Ticket Id can be used to look up the status of your ticket later,
    it is recommended to save and look up this ticket later</p>";
}

$page["title"] = "Submit Tickets";

$page["body"] = function () {
    /*    $statuses = [
            "Open",
            "In Progress",
            "Closed",
        ];*/
    $statuses = getFromDatabase::table("statuses");
    $categories = getFromDatabase::table("categories");
    ?>
    <h1>Submit Tickets</h1>
    <form action="" method="post" class="ticketSubmit frame">
        <label for="title"> Title </label>
        <input type="text" placeholder="Title" name="title" id="title">
        <label for="description" > Description </label>
        <input type="text" placeholder="Description" name="description" id="description" class="bigInput">
        <label for="category"> Category </label>
        <select name="category" id="category">
            <?php foreach ($categories as $category) { ?>
                <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
            <?php } ?>
        </select>
        <button type="submit">Submit Ticket</button>
    </form>
<?php };

include('template.php');

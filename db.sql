-- TABLES

create table categories
(
    id   int auto_increment
        primary key,
    name tinytext not null
);

create table statuses
(
    id   int auto_increment
        primary key,
    name tinytext not null
);

create table users
(
    id         int auto_increment
        primary key,
    username   tinytext                               not null,
    email      tinytext                               not null,
    password   tinytext                               not null,
    admin      tinyint(1) default 0                   not null,
    created_at timestamp  default current_timestamp() not null
);

create table tickets
(
    id          int auto_increment
        primary key,
    author_id   int                                   not null,
    title       tinytext                              not null,
    description tinytext                              not null,
    status_id   int       default 1                   not null,
    category_id int                                   not null,
    created_at  timestamp default current_timestamp() not null,
    constraint tickets_categories_id_fk
        foreign key (category_id) references categories (id),
    constraint tickets_statuses_id_fk
        foreign key (status_id) references statuses (id),
    constraint tickets_users_id_fk
        foreign key (author_id) references users (id)
);

create table messages
(
    id         int auto_increment
        primary key,
    message    mediumtext                            not null,
    author_id  int                                   not null,
    ticket_id  int                                   not null,
    created_at timestamp default current_timestamp() not null,
    constraint messages_tickets_id_fk
        foreign key (ticket_id) references tickets (id),
    constraint messages_users_id_fk
        foreign key (author_id) references users (id)
);

-- DATA

INSERT INTO statuses (id, name) VALUES (1, 'Open');
INSERT INTO statuses (id, name) VALUES (2, 'In Progress');
INSERT INTO statuses (id, name) VALUES (3, 'Dropped');
INSERT INTO statuses (id, name) VALUES (4, 'Done');

INSERT INTO categories (id, name) VALUES (1, 'Help');
INSERT INTO categories (id, name) VALUES (2, 'Review');
INSERT INTO categories (id, name) VALUES (3, 'Complaint');

INSERT INTO users (id, username, email, password, admin) VALUES (1, 'Admin', 'admin@admin.admin', '$2y$10$H3ovTA9wFIa9yA6pVU.BxuO/xCUOgveD5RYpiJvsuAb5kC0O1H/fy', 1);

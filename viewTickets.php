<?php
include "backend/getFromDatabase.php";
include "backend/submitFunctions.php";

@session_start();
if ((int)($_SESSION['user']['admin']) !== 1) {
    header("Location: searchTicket.php");
    die();
}
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    submitFunctions::updateTicket($_POST['ticketId'], $_POST['status'], $_POST['category']);
}

$page["title"] = "View Tickets";

$page["body"] = function () {
    $tickets = getFromDatabase::table("tickets");
    $users = getFromDatabase::table("users");
    $statuses = getFromDatabase::table("statuses");
    $categories = getFromDatabase::table("categories");
    $messages = getFromDatabase::table("messages");

    foreach ($tickets as $ticket) {
        $author = &$users[$ticket['author_id']];
        $status = &$statuses[$ticket['status_id']];
        $category = &$categories[$ticket['category_id']];
        ?>
        <form class="card" method="post" action="">
            <p> Ticket: <?php echo $ticket['title']; ?></p>
            <p> Author: <?php echo $author['username']; ?></p>
            <p> Status: <?php echo $status['name']; ?></p>
            <select name="status" id="status" >
                <?php foreach ($statuses as $statusIndex) { ?>
                    <option
                            value="<?php echo $statusIndex['id']; ?>"
                        <?php if ($statusIndex['id'] === $status['id']) echo "selected='selected'"; ?>
                    >
                        <?php echo $statusIndex['name']; ?>
                    </option>
                <?php } ?>
            </select>
            <p> Category: <?php echo $category['name']; ?></p>
            <select name="category" id="category" >
                <?php foreach ($categories as $categoryIndex) { ?>
                    <option
                            value="<?php echo $categoryIndex['id']; ?>"
                            <?php if ($categoryIndex['id'] === $category['id']) echo "selected='selected'"; ?>
                    >
                        <?php echo $categoryIndex['name']; ?>
                    </option>
                <?php } ?>
            </select>
            <p>Description</p>
            <p><?php echo $ticket['description']; ?></p>
            <?php // shitty way to have fixed data in $_POST ?>
            <label style="display: none">
                <input type="text" name="ticketId" value="<?php echo $ticket['id'];?>">
            </label>
            <button type="submit">Save Changes</button>
        </form>
        <hr>
    <?php }
};

include('template.php');

<?php
include "backend/loginfunctions.php";
if ($_SERVER['REQUEST_METHOD'] === 'POST') loginfunctions::signup($_POST['username'], $_POST['email'], $_POST['password'], $_POST['confirm_password']);

$page["title"] = "Sign Up";

$page["body"] = function () {?>
    <form action="" method="post" class="frame login">
        <label>
            <input type="text" placeholder="Username" name="username">
        </label>
        <label>
            <input type="email" placeholder="E-mail" name="email">
        </label>
        <label>
            <input type="password" placeholder="Password" name="password">
        </label>
        <label>
            <input type="password" placeholder="Confirm Password" name="confirm_password">
        </label>
        <button type="submit">Log In</button>
    </form>
<?php };

include('template.php');

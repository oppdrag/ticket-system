<?php
include "backend/getFromDatabase.php";

$page["title"] = "View Ticket";

$page["body"] = function () { ?>
    <form action="viewTicket.php" method="post" class="frame">
        <label for="ticketId"> Ticket ID </label>
        <input type="number" placeholder="Ticket ID" name="ticketId" id="ticketId">
        <button type="submit">Search for Ticket</button>
    </form>
<?php };

include('template.php');

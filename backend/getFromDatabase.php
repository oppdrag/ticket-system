<?php
class getFromDatabase {
    public static function table($table): array
    {
        try {
            include "backend/conn.php";
            // Fetch data from the "statuses" table
            $sql = "SELECT * FROM $table";
            $stmt = $conn->prepare($sql);
            //$stmt->bindParam(':table', $table);
            $stmt->execute();
            $tickets = [];

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $tickets[$row['id']] = $row;
            }
            return $tickets;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        return [];
    }
}
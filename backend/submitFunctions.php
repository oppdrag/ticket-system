<?php
class submitFunctions {
    public static function submitTicket(string $title, string $description, int $category_id) {
        include "backend/checkPost.php";
        try {
            include "backend/conn.php";

            $sql = "INSERT INTO tickets (title, description, category_id, author_id)
                    VALUES (:title, :description, :category_id, :author_id)";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':category_id', $category_id);
            @session_start();
            var_dump($_SESSION);
            $stmt->bindParam(':author_id', $_SESSION['user']['id']);
            $stmt->execute();
            return $conn->lastInsertId();
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
    public static function updateTicket(int $ticketId, int $statusId, int $categoryId) {
        include "backend/checkPost.php";
        try {
            include "backend/conn.php";

            $sql = "UPDATE tickets
                    set status_id = :statusId, category_id = :categoryId
                    WHERE id = :ticketId";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':ticketId', $ticketId);
            $stmt->bindParam(':statusId', $statusId);
            $stmt->bindParam(':categoryId', $categoryId);
            $stmt->execute();
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
    public static function submitMessage(string $message, int $ticket_id) {
        include "backend/checkPost.php";
        try {
            include "backend/conn.php";

            $sql = "INSERT INTO messages (message, ticket_id, author_id)
                    VALUES (:message, :ticket_id, :author_id)";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':message', $message);
            $stmt->bindParam(':ticket_id', $ticket_id);
            session_start();
            $stmt->bindParam(':author_id', $_SESSION['user']['id']);
            $stmt->execute();
            return $conn->lastInsertId();
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
    }
}

<?php
$db = [
"servername" => "localhost",
"username" => "root",
"password" => "Admin",
"dbname" => "fjelltickets",
];

try {
    $conn = new PDO("mysql:host=$db[servername];dbname=$db[dbname]", $db['username'], $db['password']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo "Tilkobling feilet: " . $e->getMessage();
}
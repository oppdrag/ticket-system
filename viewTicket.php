<?php
include "backend/getFromDatabase.php";
include "backend/submitFunctions.php";

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    header("Location: searchTicket.php");
    die();
} else if (@$_POST['command'] === "message") {
    submitFunctions::submitMessage($_POST['message'], $_POST['ticketId']);
}


$page["title"] = "View Ticket";

$page["body"] = function () {
    $tickets = getFromDatabase::table("tickets");
    $users = getFromDatabase::table("users");
    $statuses = getFromDatabase::table("statuses");
    $categories = getFromDatabase::table("categories");
    $messages = getFromDatabase::table("messages");
    $ticketId = &$_POST['ticketId'];

    if (!isset($tickets[$ticketId])) echo "<h1> No ticket with the id: $ticketId</h1>";

    $ticket = &$tickets[$ticketId];

    $author = &$users[$ticket['author_id']];
    $status = &$statuses[$ticket['status_id']];
    $category = &$categories[$ticket['category_id']];
    ?>
    <div class="frame">
        <h1>Your Ticket</h1>
        <h2> Tickets: <?php echo $ticket['title']; ?></h2>
        <h3> Author: <?php echo $author['username']; ?></h3>
        <p> Status: <?php echo $status['name']; ?></p>
        <p> Category: <?php echo $category['name']; ?></p>
        <h4>Description</h4>
        <p><?php echo $ticket['description']; ?></p>

        <hr>
        <div class="frame commentSectionParent">
            <h3>Comment Section</h3>
            <form class="frame" action="" method="post">
                <label for="message">Your Comment</label>
                <input type="text" class="bigInput" id="message" name="message" placeholder="Comment">
                <?php // Dette er shit, jeg vet ?>
                <label style="display: none">
                    <input type="text" name="ticketId" value="<?php echo $ticketId; ?>">
                    <input type="text" name="command" value="message">
                </label>
                <button type="submit">Post Comment</button>
            </form>
            <div id="commentSection">
                <?php foreach ($messages as $comment) {
                    if ((int)($comment['ticket_id']) !== (int)($ticketId)) continue;
                    ?>
                    <div class="card">
                        <p>Author: <span><?php echo $users[$comment['author_id']]['username']; ?></span></p>
                        <p><?php echo $comment['message']; ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <?php die();
};

include('template.php');

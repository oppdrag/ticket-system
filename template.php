<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title><?php echo $page["title"]; ?></title>
</head>
<body>
<nav>
    <?php @session_start(); ?>
    <?php if ((int)(@$_SESSION['user']['admin']) === 1) { ?>
        <a href="viewTickets.php">Admin View</a>
    <?php } ?>
    <a href="index.php">Home</a>
    <a href="submitTickets.php">Submit Ticket</a>
    <a href="searchTicket.php">View Ticket</a>
    <a href="login.php">Login</a>
    <?php if (isset($_SESSION['user'])) { ?>
        <a href="logout.php">Logout</a>
    <?php } ?>
</nav>
<hr>
<?php $page["body"](); ?>
</body>
</html>
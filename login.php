<?php
include "backend/loginfunctions.php";
if ($_SERVER['REQUEST_METHOD'] === 'POST') loginfunctions::login($_POST['email'], $_POST['password']);

$page["title"] = "Log In";

$page["body"] = function () {?>
    <form action="" method="post" class="frame login">
        <label>
            <input type="email" placeholder="E-mail" name="email">
        </label>
        <label>
            <input type="password" placeholder="Password" name="password">
        </label>
        <button type="submit">Log In</button>
        <a href="signup.php">don't have an acount? Make one</a>
    </form>
<?php };

include('template.php');
